import { API } from "./API"


export const fetch_all_authors = async() =>{
    try {
        const result = await API.get("/author")
        console.log("fetch_all_authors:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("fetch_all_authors error:", error);
    }
}


export const delete_author_by_id = async(id) =>{
    try {
        const result = await API.delete("/author/"+id)
        console.log("delete_author_by_id:", result);
        return result
    } catch (error) {
        console.log("delete_author_by_id error:", error);
    }
}

export const add_author = async(author) =>{
    try {
        const result = await API.post("/author", author)
        console.log("add_author:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("add_author error:", error);
    }
}