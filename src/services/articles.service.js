import { API } from "./API"


export const fetch_all_articles = async() =>{
    try {
        const result = await API.get("/article")
        console.log("fetch_all_articles:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("fetch_all_articles error:", error);
    }
}


export const delete_article_by_id = async(id) =>{
    try {
        const result = await API.delete("/article/"+id)
        console.log("delete_article_by_id:", result);
        return result
    } catch (error) {
        console.log("delete_article_by_id error:", error);
    }
}

export const add_article = async(article) =>{
    try {
        const result = await API.post("/article", article)
        console.log("add_article:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("add_article error:", error);
    }
}