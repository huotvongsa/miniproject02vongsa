import React from "react";
import Author from "./views/Author";
import Home from "./views/Home";
import Article from "./views/Article";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Menu from "./components/Menu";
import { Container } from "react-bootstrap";
import "./App.css"


export default function App() {
  return (
    <BrowserRouter>
      <Menu />
      <Container>
      <Switch>
           { <Route exact path="/" component={Home} /> }
          <Route path="/author" component={Author}/>
          <Route path="/article" component={Article}/>
        </Switch>
      </Container>
    </BrowserRouter>
  );
}
