import React from "react";
import { Container,Navbar, Nav, Button, Form, FormControl } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
export default function Menu() {
  const history = useHistory()
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#home">AMS Redux</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/article">Article</Nav.Link>
            <Nav.Link as={Link} to="/author">Author</Nav.Link>
            <Nav.Link href="#link">Category</Nav.Link>
            <Nav.Link href="#link">Language</Nav.Link>
          </Nav>
          <Form className="d-flex">
            <FormControl type="text" placeholder="Search" className="me-sm-2" />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
