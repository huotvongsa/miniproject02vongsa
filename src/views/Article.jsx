import React, { useEffect, useState } from "react";
import { Table, Button, Form,Row,Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
    addArticle,
  deleteArticleById,
  fetchAllAuthors,
} from "../redux/actions/articleAction";
import { add_article } from "../services/articles.service";

export default function Article() {
    const dispatch = useDispatch();
  const { articles } = useSelector((state) => state.articleReducer);
  //console.log("DATA:", data);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [imageURL, setImageURL] = useState("");

  useEffect(() => {
    dispatch(fetchAllArticles());
    
  }, []);

  function onDeleteArticleById(id) {
    dispatch(deleteArticleById(id));
  }

  async function onAddArticles() {
      const author = {
          title,
          description,
          thumbnail: imageURL
      }
    dispatch(addArticle(article))
    //const result = await add_tutorial(tutorial)
    //dispatch(fetchAllTutorials());

  }
    
    return (
        <>
            <div className="d-flex justify-content-between my-4">
                <h1>Author</h1>
            </div>
            <div>
            <Form>
                <Row>
                    <Col sm={9}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Control
                        type="text"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        placeholder="Name"
                        />
                        <Form.Text className="text-muted"></Form.Text>
                    </Form.Group>
                <br />
                    <Form.Group controlId="formBasicEmail">
                    <Form.Control
                        type="text"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Eamil"
                    />
                    <Form.Text className="text-muted"></Form.Text>
                    </Form.Group>
                </Col>
                    <Col sm={3}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Control
                        type="text"
                        value={imageURL}
                        onChange={(e) => setImageURL(e.target.value)}
                        placeholder="Thumbnail url"
                        />
                        <Form.Text className="text-muted"></Form.Text>
                    </Form.Group>

                <br />
                <Button onClick={onAddAuthor} variant="primary" type="button">
                    Add
                </Button>
                    </Col>
                </Row>
            
                <br />
            
            </Form>
        </div>
        </>
    )
}

