import React, { useEffect, useState } from "react";
import { Table, Button, Form,Row,Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
    addAuthor,
  deleteAuthorById,
  fetchAllAuthors,
} from "../redux/actions/authorAction";
import { add_author } from "../services/authors.service";

export default function Home() {
  const dispatch = useDispatch();
  const { authors } = useSelector((state) => state.authorReducer);
  //console.log("DATA:", data);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [imageURL, setImageURL] = useState("");

  useEffect(() => {
    dispatch(fetchAllAuthors());
    
  }, []);

  function onDeleteAuthorById(id) {
    dispatch(deleteAuthorById(id));
  }

  async function onAddAuthor() {
      const author = {
          name,
          email,
          thumbnail: imageURL
      }
    dispatch(addAuthor(author))
    //const result = await add_tutorial(tutorial)
    //dispatch(fetchAllTutorials());

  }

  return (
    <>
      <div className="d-flex justify-content-between my-4">
        <h1>Author</h1>
      </div>
      <div>
        
        <Form>
          <Row>
            <Col sm={9}>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  placeholder="Name"
                />
                <Form.Text className="text-muted"></Form.Text>
            </Form.Group>
          <br />
            <Form.Group controlId="formBasicEmail">
              <Form.Control
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="Eamil"
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>
          </Col>
            <Col sm={3}>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="text"
                  value={imageURL}
                  onChange={(e) => setImageURL(e.target.value)}
                  placeholder="Thumbnail url"
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>

          <br />
          <Button onClick={onAddAuthor} variant="primary" type="button">
            Add
          </Button>
            </Col>
          </Row>
          
          <br />
          
        </Form>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Thumbnail</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {authors.map((item, index) => (
            <tr key={index}>
              <td>{item._id.slice(0, 8)}</td>
              <td>{item.name}</td>
              <td>{item.email}</td>
              <td>
                <img width="200px" src={item.thumbnail} />
              </td>
              <td>
                <Button variant="primary">View</Button>{" "}
                <Button variant="warning">Edit</Button>{" "}
                <Button
                  onClick={() => onDeleteAuthorById(item._id)}
                  variant="danger"
                >
                  Delete
                </Button>{" "}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}
