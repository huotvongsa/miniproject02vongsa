import React, { useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
    addArticle,
  deleteArticleById,
  fetchAllArticles,
} from "../redux/actions/articleAction";
import { add_article } from "../services/articles.service";

export default function Home() {
  const dispatch = useDispatch();
  const { articles } = useSelector((state) => state.articleReducer);
  //console.log("DATA:", data);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [imageURL, setImageURL] = useState("");

  useEffect(() => {
    dispatch(fetchAllArticles());
    
  }, []);

  function onDeleteArticleById(id) {
    dispatch(deleteArticleById(id));
  }

  async function onAddArticles() {
      const article = {
          title,
          description,
          thumbnail: imageURL
      }
    dispatch(addArticle(article))
    //const result = await add_tutorial(tutorial)
    //dispatch(fetchAllTutorials());

  }

  return (
    <>
    {articles.map((item, index) => (
    <Card style={{ width: '18rem' }} key={index}>
        <Card.Img variant="top" src={item.thumbnail} />
        <Card.Body>
            <Card.Title>{item.title}</Card.Title>
            <Card.Text>
            {item.description}
            </Card.Text>
            <Button variant="primary">View</Button>{" "}
            <Button variant="warning">Edit</Button>{" "}
            <Button
                onClick={() => onDeleteArticleById(item._id)}
                variant="danger"
                >
                  Delete
            </Button>{" "}
        </Card.Body>
    </Card>
    ))}       
    </>
  );
}
