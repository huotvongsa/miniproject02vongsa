import Swal from "sweetalert2";
import {
    add_article,
  delete_article_by_id,
  fetch_all_articles,
} from "../../services/articles.service";

export const FETCH_ALL_ARTICLES = "FETCH_ALL_ARTICLE";
export const DELETE_ARTICLE_BY_ID = "DELETE_ARTICLE_BY_ID";
export const ADD_ARTICLE = "ADD_ARTICLE";

export const fetchAllArticles = () => {
  return async (dispatch) => {
    const result = await fetch_all_articles();
    dispatch({
      type: FETCH_ALL_ARTICLES,
      payload: result,
    });
  };
};

export const addArticle = (article) => {
    return async (dispatch) => {
      const result = await add_article(article);
      dispatch({
        type: ADD_ARTICLE,
        payload: result,
      });
    };
  };

export const deleteArticleById = (id) => {
  return async (dispatch) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const result = await delete_article_by_id(id);
        dispatch({
          type: DELETE_ARTICLE_BY_ID,
          payload: id,
        });
        Swal.fire("Deleted!", result.data.message, "success");
      }
    });
  };
};
