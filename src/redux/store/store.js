import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import authorReducer from '../reducers/authorReducer'

const rootReducers = combineReducers({
    authorReducer,
})

export const store = createStore(rootReducers, applyMiddleware(thunk))