import { ADD_ARTICLE, DELETE_ARTICLE_BY_ID, FETCH_ALL_ARTICLES } from "../actions/articleAction"

const initState = {
    articles: [],
}

export default function articleReducer(state = initState, {type, payload}){
    switch(type){
        case FETCH_ALL_ARTICLES:
            return{
                ...state,
                articles: payload
            }
        case DELETE_ARTICLE_BY_ID:
            return{
                ...state,
                articles: state.articles.filter(item=>item._id != payload)
            }
        case ADD_ARTICLE:
            return{
                ...state,
                articles: [...state.articles, payload]
            }
        default:
            return state
    }

}