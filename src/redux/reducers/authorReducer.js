import { ADD_AUTHOR, DELETE_AUTHOR_BY_ID, FETCH_ALL_AUTHORS } from "../actions/authorAction"

const initState = {
    authors: [],
}

export default function authorReducer(state = initState, {type, payload}){
    //console.log("tutorialReducer action:", action);
    switch(type){
        case FETCH_ALL_AUTHORS:
            return{
                ...state,
                authors: payload
            }
        case DELETE_AUTHOR_BY_ID:
            return{
                ...state,
                authors: state.authors.filter(item=>item._id != payload)
            }
        case ADD_AUTHOR:
            return{
                ...state,
                authors: [...state.authors, payload]
            }
        default:
            return state
    }

}